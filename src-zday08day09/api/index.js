import ajax from "@/utils/request";
import mockAjax from "@/utils/mockRequest";


//首页三级分类  GET   /api/product/getBaseCategoryList  无参数
// eslint-disable-next-line no-unused-vars
export const reqCategoryList = () => ajax({
    method: 'get',
    url: '/product/getBaseCategoryList'
})

//5. 获取商品详情 api/item/{ skuId }  GET              skuId	string	Y	商品ID
export const reqGoodsDetailInfo = (skuId) =>ajax({
    method:'get',
    url: `/item/${skuId}`,

})

//4. 搜索商品  /api/list  POST
//参数类型 请求体参数
// {
//     "category3Id": "61",
//     "categoryName": "手机",
//     "keyword": "小米",
//     "order": "1:desc",
//     "pageNo": 1,
//     "pageSize": 10,
//     "props": ["1:1700-2799:价格", "2:6.65-6.74英寸:屏幕尺寸"],
//     "trademark": "4:小米"
// }
export const reqGoodsListInfo = (searchParamsObj) => ajax({
    method: 'post',
    url: '/list',
    data: searchParamsObj

})


//7. 添加到购物车(对已有物品进行数量改动)  /api/cart/addToCart/{ skuId }/{ skuNum }  POST
// skuID	string	Y	商品ID
// skuNum	string	Y	商品数量
//                         正数代表增加
//                         负数代表减少
export const reqAddOrUpdateShopCart = (skuId,skuNum) => ajax({
    method: 'POST',
    url: `/cart/addToCart/${skuId}/${skuNum}`
})

//6. 获取购物车列表  /api/cart/cartList   GET  无参数
export const reqShopCartList = () => ajax({
    method: 'get',
    url: '/cart/cartList'
})
//8. 切换商品选中状态 /api/cart/checkCart/{skuID}/{isChecked}
//get
// skuId	string	Y	商品ID
// isChecked	string	Y	商品选中状态
//                          0代表取消选中
//                          1代表选中

export const reqChangeOneIsChecked = (skuId,isChecked) => {
    return ajax({
        method:'get',
        url:`/cart/checkCart/${skuId}/${isChecked}`
    })

}
//批量选中购物车 	post   /api/cart/batchCheckCart/{isChecked}
// 参数：skuIdList  数组  代表修改的商品id列表     请求体参数
// isChecked  要修改的状态   1代表选中  0代表未选中

export const reqChangeAllIsChecked = (skuIdList,isChecked) => {
    return ajax({
        method:'post',
        url:`/cart/batchCheckCart/${isChecked}`,
        data:skuIdList
    })
}
//9. 删除购物车商品单个  /api/cart/deleteCart/{skuId}  DELETE
export const reqRemoveOneGoods = (skuId) => {
    return ajax({
        method:'delete',
        url:`/cart/deleteCart/${skuId}`
    })
}
//批量删除购物车  	DELETE  /api/cart/batchDeleteCart
//参数：skuIdList  数组  代表修改的商品id列表     请求体参数
export const reqRemoveAllCheckedGoods = (skuIdList) => {
    return ajax({
        method:'delete',
        url:'/cart/batchDeleteCart',
        data:skuIdList
    })
}
//获取验证码  /api/user/passport/sendCode/{phone}  获取验证码  get

export const reqCode = (phone) => {
    return ajax({
        method:'get',
        url:`/user/passport/sendCode/${phone}`,
    })
}
///16. 注册用户  /api/user/passport/register  POST
// 参数名称	类型	是否必选	描述
// phone	string	Y	注册手机号
// password	string	Y	密码
// code	string	Y	验证码

export const reqUserRegister = (userInfo) => {
    return ajax({
        method:'post',
        url:`/user/passport/register`,
        data:userInfo
    })
}
//2. 登录  /api/user/passport/login  POST
// phone	string	Y	用户名
// password	string	Y	密码

export const reqUserLogin = (userInfo) => {
    return ajax({
        method:'post',
        url:`/user/passport/login`,
        data:userInfo
    })
}
//获取用户信息   /api/user/passport/auth/getUserInfo  get
export const reqUserInfo = () => {
    return ajax({
        method:'get',
        url:`/user/passport/auth/getUserInfo`,
    })
}
//17. 退出登陆 /api/user/passport/logout   GET
export const reqUserLogout = () => {
    return ajax({
        method:'get',
        url:`/user/passport/logout`,
    })
}






//mock模拟数据
export const reqFloorList = () => mockAjax({
    method: 'get',
    url: '/mock/floor'
})

export const reqBannerList = () => mockAjax({
    method: 'get',
    url: '/mock/banner'
})

