//这个文件是专门封装axios的
//最终达到的效果
// 			配置基础路径和超时限制
// 			添加进度条信息  nprogress
// 			返回的响应不再需要从data属性当中拿数据，而是响应就是我们要的数据
// 			统一处理请求错误, 具体请求也可以选择处理或不处理

import axios from "axios";
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import store from '@/store';
//创建一个axios的实例
const service = axios.create({
    baseURL: '/api',
    timeout: 5000,
});

// Add a request interceptor
service.interceptors.request.use(function (config) {
    NProgress.start();
    const userTempId=store.state.user.userTempId
    const token=store.state.user.token
    console.log(token)
    if (userTempId){
        config.headers.userTempId=userTempId
    }
    if (token){
        config.headers.token=token
    }

    return config;
}, function (error) {


    return Promise.reject(error);
});


// Add a response interceptor
service.interceptors.response.use(function (response) {
    NProgress.done();


    return response.data;
}, function (error) {
    NProgress.done();

    alert('请求失败'+error.message);
    // return Promise.reject(error);
    //后续如果想继续处理的话
    // return Promise.reject(new Error(error.message));

    //如果后续不想继续处理  使用pending状态的的promise来终止promise链的传递
    return new Promise(() => {})
});

export default service;
