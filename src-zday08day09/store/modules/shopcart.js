import {
    reqAddOrUpdateShopCart,
    reqChangeOneIsChecked,
    reqChangeAllIsChecked,
    reqShopCartList,
    reqRemoveOneGoods, reqRemoveAllCheckedGoods
} from "@/api";


const state = {
    shopCartList: []
}
const mutations = {
    SET_SHOP_CART_LIST(state, shopCartList) {
        state.shopCartList = shopCartList
    }
}
const actions = {
    // eslint-disable-next-line no-unused-vars
    async addOrUpdateShopCart({commit},{skuId,skuNum}){
        const result=await reqAddOrUpdateShopCart(skuId,skuNum)
        if(result.code === 200){
            // console.log('ok')
            return 'ok'

        }else {
            return Promise.reject(new Error('failed'))
        }
    },
    //获取购物车列表
    async getShopCartList({commit}) {
        const result=await reqShopCartList()
        if(result.code === 200){
            commit('SET_SHOP_CART_LIST',result.data)
        }
    },
    //切换单个商品选中状态
    // eslint-disable-next-line no-unused-vars
    async getChangeOneIsChecked({commit},{skuId,isChecked}){
        const result = await reqChangeOneIsChecked(skuId,isChecked)
        if(result.code === 200){
            return 'ok'
        }else {
            return Promise.reject(new Error('failed'))
        }
    },
    //批量修改购物车选中状态
    // eslint-disable-next-line no-unused-vars
    async getChangeALlIsChecked({commit},{skuIdList,isChecked}){
        const result = await reqChangeAllIsChecked(skuIdList,isChecked)
        if (result.code === 200){
            return 'ok'
        }else {
            return Promise.reject(new Error('failed'))
        }
    },
    //删除购物车商品单个
    // eslint-disable-next-line no-unused-vars
    async getRemoveOneGoods({commit},skuId){
        const result=await reqRemoveOneGoods(skuId)
        if (result.code === 200){
            return 'ok'
        }else {
            return Promise.reject(new Error('failed'))
        }
    },
    //删除购物车选中商品
    // eslint-disable-next-line no-unused-vars
    async getRemoveAllCheckedGoods({commit},skuIdList){
        const result=await reqRemoveAllCheckedGoods(skuIdList)
        if(result.code === 200){
            return 'ok'
        }else {
            return Promise.reject(new Error('failed'))
        }
    },
}

const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}
