import Vue from "vue";
import VueRouter from "vue-router";

import routes from './routes.js';
import store from '@/store';


Vue.use(VueRouter);
//为了避免router上的push方法连续请求相同的参数会出现报错的情况，我们重写push方法
const originPush=VueRouter.prototype.push;
const originReplace=VueRouter.prototype.replace;
VueRouter.prototype.push=function(location,resolve,reject){

    if (resolve===undefined&&reject===undefined){
        return originPush.call(this,location).catch(() => {});
    }else {
        return originPush.call(this,location,resolve,reject);
    }

}

VueRouter.prototype.replace=function(location,resolve,reject){

    if (resolve===undefined&&reject===undefined){
        return originReplace.call(this,location).catch(() => {});
    }else {
        return originReplace.call(this,location,resolve,reject);
    }

}
const router=new VueRouter({
    mode:'history',

    routes,
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }


})

router.beforeEach(async (to, from, next)=>{
    // to  代表的是目的地的路由对象
    // from 代表的是起始页面的路由对象
    // next 代表的是一个函数，可以控制你是放行还是处理
    // next()  如果不传参代表无条件放行
    // next(false) 如果传递false代表不放行，原地待命
    // next('/') 如果传递的是一个路径或者是路由对象，那么可以强制让路由跳转到指定的位置
    const token = store.state.user.token
    const userInfo = store.state.user.userInfo

    if (token){
        //如果token存在，还去登陆页的话，直接强制跳转首页
        if (to.path==='/login'){
            next('/home')
        }else {
            //token存在，不去往登陆页面
            if (userInfo.name){
                //如果有token有用户信息，则想去哪儿就去哪儿
                next()
            }else {
                //有token没有用户信息，则发送请求获取用户信息
                try {
                    await store.dispatch('getUserInfo')
                    //获取成功后放行
                    next()

                } catch (error) {
                    //获取用户信息失败后，我们统一认为是token过期失效了，让他重新登录获取token
                    //先清除所有失效的token
                    store.dispatch('getRemoveTokenAndUserInfo')
                    next('/login')

                }


            }


        }


    }else {
        //还没做支付逻辑，先放行
        next();
    }


})






export default router
