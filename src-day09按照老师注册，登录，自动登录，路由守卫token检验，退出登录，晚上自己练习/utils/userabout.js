import { v4 as uuidv4 } from 'uuid';
//获取用户临时唯一标识
export function getUserTempId() {

    let userTempId=localStorage.getItem('userTempId_key');
    if(!userTempId){
        userTempId=uuidv4(); // ⇨ '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d'
        localStorage.setItem('userTempId_key',userTempId)
    }
    return userTempId;

}
