import {reqGoodsDetailInfo} from "@/api";

const state = {
    goodsDetailInfo: {}
}
const mutations = {
    SET_GOODS_DETAIL_INFO(state, goodsDetailInfo) {
        state.goodsDetailInfo = goodsDetailInfo
    }

}
const actions = {
    async getGoodsDetailInfo({commit}, skuId) {
        const result = await reqGoodsDetailInfo(skuId)
        if (result.code === 200) {
            commit('SET_GOODS_DETAIL_INFO', result.data)
        }
    }

}
const getters = {
    categoryView(state) {
        return state.goodsDetailInfo.categoryView || {} //避免拿到undefined
    },
    skuInfo() {
        return state.goodsDetailInfo.skuInfo || {} //避免拿到undefined
    },
    spuSaleAttrList() {
        return state.goodsDetailInfo.spuSaleAttrList || [] //避免拿到undefined
    }

}

export default {
    state,
    mutations,
    actions,
    getters
}
