import ajax from "@/utils/request";
import mockAjax from "@/utils/mockRequest";


//首页三级分类  GET   /api/product/getBaseCategoryList  无参数
// eslint-disable-next-line no-unused-vars
export const reqCategoryList = () => ajax({
    method: 'get',
    url: '/product/getBaseCategoryList'
})

//4. 搜索商品  /api/list  POST
//参数类型 请求体参数
// {
//     "category3Id": "61",
//     "categoryName": "手机",
//     "keyword": "小米",
//     "order": "1:desc",
//     "pageNo": 1,
//     "pageSize": 10,
//     "props": ["1:1700-2799:价格", "2:6.65-6.74英寸:屏幕尺寸"],
//     "trademark": "4:小米"
// }
export const reqGoodsListInfo = (searchParamsObj) => ajax({
    method: 'post',
    url: '/list',
    data: searchParamsObj

})

//5. 获取商品详情 api/item/{ skuId }  GET              skuId	string	Y	商品ID
export const reqGoodsDetailInfo = (skuId) => ajax({
    method: 'get',
    url: `/item/${skuId}`,

})

//7. 添加到购物车(对已有物品进行数量改动)  /api/cart/addToCart/{ skuId }/{ skuNum }  POST
// skuID	string	Y	商品ID
// skuNum	string	Y	商品数量
//                         正数代表增加
//                         负数代表减少
export const reqAddOrUpdateShopCart = (skuId, skuNum) => ajax({
    method: 'POST',
    url: `/cart/addToCart/${skuId}/${skuNum}`
})
// 6. 获取购物车列表  /api/cart/cartList  get  无参数
export const reqShopCartList = () => ajax({
    method: 'GET',
    url: '/cart/cartList'
})
//8. 切换商品选中状态  /api/cart/checkCart/{skuID}/{isChecked}  GET
export const reqChangeOneIsChecked = (skuId,isChecked) => ajax({
    method: 'get',
    url:`/cart/checkCart/${skuId}/${isChecked}`
})
//批量选中购物车 /api/cart/batchCheckCart/{isChecked}   post   skuIdList  数组  代表修改的商品id列表     请求体参数
// 	                                                          isChecked  要修改的状态   1代表选中  0代表未选中

export const reqChangeAllIsChecked = (skuIdList,isChecked) => ajax({
    method: 'post',
    url:`/cart/batchCheckCart/${isChecked}`,
    data:skuIdList
})
//9. 删除购物车商品 单个  /api/cart/deleteCart/{skuId}  DELETE
//参数名称	类型	是否必选	描述
// skuId	string	Y	商品id
export const reqDeleteOneGoods=(skuId)=>ajax({
    method:'delete',
    url:`cart/deleteCart/${skuId}`,
})
//删除购物车选中商品  	/api/cart/batchDeleteCart DELETE  参数：skuIdList  数组  代表修改的商品id列表     请求体参数
export const reqDeleteCheckedGoods = (skuIdList) => ajax({
    method:'delete',
    url:'/cart/batchDeleteCart',
    data:skuIdList
})
// 获取验证码  api/user/passport/sendCode/{phone}
// get
export const reqCode = (phone) => {
  return ajax({
      method:'get',
      url: `/user/passport/sendCode/${phone}`,
  })
}
//16. 注册用户  /api/user/passport/register  POST
// 参数名称	类型	是否必选	描述
// phone	string	Y	注册手机号
// password	string	Y	密码
// code	string	Y	验证码
export const reqUserRegister = (userInfo) => {
    return ajax({
        method:'post',
        url: '/user/passport/register',
        data:userInfo
    })
}

//2. 登录  /api/user/passport/login   POST
// 参数名称	类型	是否必选	描述
// phone	string	Y	用户名
// password	string	Y	密码


export const reqUserLogin = (userInfo) => {
    return ajax({
        method:'post',
        url: '/user/passport/login',
        data:userInfo
    })
}
//api/user/passport/auth/getUserInfo 添加了token校验获取用户登录信息，用户登录只保存用户的token
//get

export const reqUserInfo = () => {
    return ajax({
        method:'get',
        url: '/user/passport/auth/getUserInfo',
    })
}
//17. 退出登陆  /api/user/passport/logout  GET
export const reqUserLogout = () => {
    return ajax({
        method:'get',
        url: '/user/passport/logout',

    })
}



//mock模拟数据
export const reqFloorList = () => mockAjax({
    method: 'get',
    url: '/mock/floor'
})

export const reqBannerList = () => mockAjax({
    method: 'get',
    url: '/mock/banner'
})

