import Vue from "vue";
import VueRouter from "vue-router";

import store from '@/store'
import routes from './routes.js'


Vue.use(VueRouter);
//为了避免router上的push方法连续请求相同的参数会出现报错的情况，我们重写push方法
const originPush = VueRouter.prototype.push;
const originReplace = VueRouter.prototype.replace;
VueRouter.prototype.push = function (location, resolve, reject) {

    if (resolve === undefined && reject === undefined) {
        return originPush.call(this, location).catch(() => {
        });
    } else {
        return originPush.call(this, location, resolve, reject);
    }

}

VueRouter.prototype.replace = function (location, resolve, reject) {

    if (resolve === undefined && reject === undefined) {
        return originReplace.call(this, location).catch(() => {
        });
    } else {
        return originReplace.call(this, location, resolve, reject);
    }

}

const router = new VueRouter({
    mode: 'history',

    routes,
    scrollBehavior(to, from, savedPosition) {
        return {x: 0, y: 0}
    }


})

router.beforeEach(async (to, from, next) => {
    // to  代表的是目的地的路由对象
    // from 代表的是起始页面的路由对象
    // next 代表的是一个函数，可以控制你是放行还是处理
    // next()  如果不传参代表无条件放行
    // next(false) 如果传递false代表不放行，原地待命
    // next('/') 如果传递的是一个路径或者是路由对象，那么可以强制让路由跳转到指定的位置
    const token = store.state.user.token
    const userInfo = store.state.user.userInfo
    //如果token存在
    if (token) {

        if (to.path === '/login') {
            //存在token还去往登陆页面，直接强制跳转到首页
            next('/')
        }else {
            //如果不是点击登陆页面
            if (userInfo.name){
                // 如果用户信息存在的话，想去哪儿就去哪儿
                next()
            }else {
                //如果存在token 没有用户信息 那么发请求拿用户信息
                try {
                    await store.dispatch('getUserInfo')
                    //成功拿到信息的话放行
                    next()
                } catch (error) {
                    //没有拿到用户信息的话，我们一律认为是token过期了，重新去往登陆页面登陆获取token
                    //清除token信息
                    store.dispatch('getRemoveTokenAndUserInfo')

                    next('/login')

                }


            }





        }

    } else {
        //目前我们没做支付逻辑，等到后面在写
        next()
    }

})


export default router
