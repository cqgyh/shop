import Vue from 'vue';
import Vuex from 'vuex';

import home from "@/store/modules/home";
import user from "@/store/modules/user";

import state from "@/store/state";
import mutations from "@/store/mutations";
import actions from "@/store/actions";
import getters from "@/store/getters";
import search from "@/store/modules/search";
// const state = {}
// const mutations = {}
// const actions = {}
// const getters = {}



Vue.use(Vuex);


export default new Vuex.Store({

    state,
    mutations,
    actions,
    getters,

    //合并小的module到总的Store
    modules: {
        home,
        user,
        search
    }

})
