import ajax from "@/utils/request";
import mockAjax from "@/utils/mockRequest";



//首页三级分类  GET   /api/product/getBaseCategoryList  无参数
// eslint-disable-next-line no-unused-vars
export const reqCategoryList = () => ajax({
    method: 'get',
    url: '/product/getBaseCategoryList'
})

//5. 获取商品详情 api/item/{ skuId }  GET              skuId	string	Y	商品ID
export const reqGoodsDetailInfo = (skuId) =>ajax({
    method:'get',
    url: `/item/${skuId}`,

})

//4. 搜索商品  /api/list  POST
//参数类型 请求体参数
// {
//     "category3Id": "61",
//     "categoryName": "手机",
//     "keyword": "小米",
//     "order": "1:desc",
//     "pageNo": 1,
//     "pageSize": 10,
//     "props": ["1:1700-2799:价格", "2:6.65-6.74英寸:屏幕尺寸"],
//     "trademark": "4:小米"
// }
export const reqGoodsListInfo = (searchParamsObj) => ajax({
    method: 'post',
    url: '/list',
    data: searchParamsObj

})


//7. 添加到购物车(对已有物品进行数量改动)  /api/cart/addToCart/{ skuId }/{ skuNum }  POST
// skuID	string	Y	商品ID
// skuNum	string	Y	商品数量
//                         正数代表增加
//                         负数代表减少
export const reqAddOrUpdateShopCart = (skuId,skuNum) => ajax({
    method: 'POST',
    url: `/cart/addToCart/${skuId}/${skuNum}`
})


//6. 获取购物车列表  /api/cart/cartList GET 无参数

export const reqShopCartList = () => ajax({
    method: 'GET',
    url: `/cart/cartList`
})
//8. 切换单个商品选中状态  /api/cart/checkCart/{skuID}/{isChecked}  GET
// skuId	string	Y	商品ID
// isChecked	string	Y	商品选中状态
//                          0代表取消选中
//                           1 代表选中
export const reqChangeOneIsChecked = (skuId,isChecked) => ajax({
    method:'get',
    url:`/cart/checkCart/${skuId}/${isChecked}`
})

//批量选中购物车  	post  /api/cart/batchCheckCart/{isChecked}
// 参数：skuIdList  数组  代表修改的商品id列表     请求体参数
//      isChecked  要修改的状态   1代表选中  0代表未选中
export const reqChangeAllIsChecked = (skuIdList,isChecked) => ajax({
    method:'POST',
    url: `cart/batchCheckCart/${isChecked}`,
    data:skuIdList
})

//9. 删除购物车商品单个 /api/cart/deleteCart/{skuId}  DELETE
//参数名称	类型	是否必选	描述
// skuId	string	Y	商品id
export const reqDeleteOneGoods = (skuId) => ajax({
    method:'DELETE',
    url:`/cart/deleteCart/${skuId}`
})
//删除购物车选中的商品 	DELETE  /api/cart/batchDeleteCart
//参数：skuIdList  数组  代表修改的商品id列表     请求体参数
export const reqDeleteALlGoods = (skuIdList) => ajax({
    method:'DELETE',
    url:`/cart/batchDeleteCart`,
    data:skuIdList
})










//mock模拟数据
export const reqFloorList = () => mockAjax({
    method: 'get',
    url: '/mock/floor'
})

export const reqBannerList = () => mockAjax({
    method: 'get',
    url: '/mock/banner'
})

