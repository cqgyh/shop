import {reqCategoryList} from '@/api';

const state = {

    categoryList: [],
}
const mutations = {
    SET_CATEGORY_LIST(state, categoryList) {
        state.categoryList = categoryList
    }

}
const actions = {
    //用这个发请求
    async getCategoryList({commit}) {
        const result = await reqCategoryList();

        if (result.code === 200) {
            commit('SET_CATEGORY_LIST', result.data)
        }
    }


}
const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}
