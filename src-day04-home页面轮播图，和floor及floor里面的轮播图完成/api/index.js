import ajax from "@/utils/request";
import mockAjax from "@/utils/mockRequest";


//首页三级分类  GET   /api/product/getBaseCategoryList  无参数
// eslint-disable-next-line no-unused-vars
export const reqCategoryList = () => ajax({
    method: 'get',
    url: '/product/getBaseCategoryList'
})

// reqCategoryList()




//mock模拟数据
export const reqFloorList = () => mockAjax({
    method: 'get',
    url: '/mock/floor'
})

export const reqBannerList = () => mockAjax({
    method: 'get',
    url: '/mock/banner'
})

