import {reqGoodsListInfo} from "@/api";

const state = {
    goodsListInfo:{}

}
const mutations = {
    SET_GOODS_LIST_INFO(state,goodsListInfo){
        state.goodsListInfo=goodsListInfo
    }
}
const actions = {
   async getGoodsListInfo({commit},searchParamsObj){
        const  result=await reqGoodsListInfo(searchParamsObj);
        if(result.code === 200){
        commit('SET_GOODS_LIST_INFO',result.data)
        }
   }



}
const getters = {

    attrsList(state){
        return state.goodsListInfo.attrsList||[] //如果没有传递一个空数组，避免遍历的时候报错
    },
    goodsList(state){
        return state.goodsListInfo.goodsList||[] //如果没有传递一个空数组，避免遍历的时候报错
    },
    trademarkList(state){
        return state.goodsListInfo.trademarkList||[] //如果没有传递一个空数组，避免遍历的时候报错
    }

}

export default {
    state,
    mutations,
    actions,
    getters
}
