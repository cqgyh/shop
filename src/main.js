import Vue from 'vue'
import App from './App'

import router from '@/router';
import store from '@/store';

//mock模拟测试数据
import '@/mock/mockServer'

import TypeNav from "@/components/TypeNav/TypeNav";
import Pagination from "@/components/Pagination";
import * as api from '@/api'
//引入验证
import '@/utils/validate'
//图片懒加载
import VueLazyload from 'vue-lazyload';
import loading from '@/assets/images/loading.gif'
// 在图片界面没有进入到可视范围前不加载, 在没有得到图片前先显示loading图片
Vue.use(VueLazyload, { // 内部自定义了一个指令lazy
  loading,  // 指定未加载得到图片之前的loading图片
})
// // 配置项
// Vue.use(VueLazyload, {
//   // preLoad: 1.3,
//   // error: 'dist/error.png',
//   // loading: 'dist/loading.gif',
//   // error: 'assets/images/loading.gif',
//   loading: '@/assets/images/loading.gif'
//   // attempt: 1
// })

// import '@/api'  //第一种测试接口请求函数
//第二种测试接口请求函数
// import {reqCategoryList} from '@/api'
// reqCategoryList()
//定义全局组件
Vue.component('TypeNav',TypeNav)
Vue.component('Pagination',Pagination)

//element-ui按需引入
import { Button,MessageBox, Message,Loading,Notification} from 'element-ui';
Vue.use(Button)


Vue.prototype.$loading = Loading.service;
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;


Vue.config.productionTip = false


new Vue({
  beforeCreate() {
    Vue.prototype.$bus=this;
    Vue.prototype.$api=api;
  },
  router,
  store,
  render: h => h(App),
}).$mount('#app')
