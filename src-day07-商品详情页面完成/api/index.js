import ajax from "@/utils/request";
import mockAjax from "@/utils/mockRequest";


//首页三级分类  GET   /api/product/getBaseCategoryList  无参数
// eslint-disable-next-line no-unused-vars
export const reqCategoryList = () => ajax({
    method: 'get',
    url: '/product/getBaseCategoryList'
})

//4. 搜索商品  /api/list  POST
//参数类型 请求体参数
// {
//     "category3Id": "61",
//     "categoryName": "手机",
//     "keyword": "小米",
//     "order": "1:desc",
//     "pageNo": 1,
//     "pageSize": 10,
//     "props": ["1:1700-2799:价格", "2:6.65-6.74英寸:屏幕尺寸"],
//     "trademark": "4:小米"
// }
export const reqGoodsListInfo = (searchParamsObj) =>ajax({
    method:'post',
    url: '/list',
    data:searchParamsObj

})
//5. 获取商品详情 api/item/{ skuId }  GET              skuId	string	Y	商品ID
export const reqGoodsDetailInfo = (skuId) =>ajax({
    method:'get',
    url: `/item/${skuId}`,

})
//添加购物车的请求
// /api/cart/addToCart/{ skuId }/{ skuNum }
// post

export const reqAddOrUpdateShopCart = (skuId,skuNum) => {
    return ajax({
        url:`/cart/addToCart/${ skuId }/${ skuNum }`,
        method:'post'
    })
}




//mock模拟数据
export const reqFloorList = () => mockAjax({
    method: 'get',
    url: '/mock/floor'
})

export const reqBannerList = () => mockAjax({
    method: 'get',
    url: '/mock/banner'
})

