import {reqGoodsDetailInfo} from "@/api";

const state = {
    goodsDetailInfo:{}
}
const mutations = {
    SET_GOODS_DETAIL_INFO(state,goodsDetailInfo){
        state.goodsDetailInfo=goodsDetailInfo
    }
}
const actions = {
    async getGoodsDetailInfo({commit},skuId) {
        const result=await reqGoodsDetailInfo(skuId)
        if (result.code === 200){
            commit('SET_GOODS_DETAIL_INFO',result.data)

        }
    }

}
const getters = {
    categoryView(state){
        return state.goodsDetailInfo.categoryView||{} //如果没有传递一个空对象，避免报错
    },
    spuSaleAttrList(state){
        return state.goodsDetailInfo.spuSaleAttrList||[] //如果没有传递一个空对象，避免报错
    },
    skuInfo(state){
        return state.goodsDetailInfo.skuInfo||{} //如果没有传递一个空对象，避免报错
    },



}

export default {
    state,
    mutations,
    actions,
    getters
}
