import {reqBannerList, reqCategoryList, reqFloorList} from '@/api';

const state = {

    categoryList: [],
    bannerList:[],
    floorList:[],
}
const mutations = {
    SET_CATEGORY_LIST(state, categoryList) {
        state.categoryList = categoryList
    },
    SET_BANNER_LIST(state, bannerList) {
        state.bannerList = bannerList
    },
    SET_FLOOR_LIST(state, floorList) {
        state.floorList = floorList
    }


}
const actions = {
    //用这个发请求
    async getCategoryList({commit}) {
        const result = await reqCategoryList();
        if (result.code === 200) {
            commit('SET_CATEGORY_LIST', result.data)
        }
    },
    async getBannerList({commit}) {
        const result = await reqBannerList();
        if (result.code === 200) {
            commit('SET_BANNER_LIST', result.data)
        }
    },
    async getFloorList({commit}) {
        const result = await reqFloorList();
        if (result.code === 200) {
            commit('SET_FLOOR_LIST', result.data)
        }
    },


}
const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}
