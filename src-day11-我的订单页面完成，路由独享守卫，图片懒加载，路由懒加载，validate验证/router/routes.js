// import Home from "@/pages/Home";
// import Login from "@/pages/Login";
// import Register from "@/pages/Register";
// import Search from "@/pages/Search";
// import Detail from "@/pages/Detail";
// import AddCartSuccess from "@/pages/AddCartSuccess";
// import ShopCart from "@/pages/ShopCart";
// import Pay from "@/pages/Pay";
// import PaySuccess from "@/pages/PaySuccess";
// import Center from "@/pages/Center";
// import Trade from "@/pages/Trade";
// import MyOrder from "@/pages/Center/MyOrder/MyOrder";
// import GroupOrder from "@/pages/Center/GroupOrder/GroupOrder";

const Home = () => import("@/pages/Home");
const Login = () => import("@/pages/Login");
const Register = () => import("@/pages/Register");
const Search = () => import("@/pages/Search");
const Detail = () => import("@/pages/Detail");
const AddCartSuccess = () => import("@/pages/AddCartSuccess");
const ShopCart = () => import("@/pages/ShopCart");
const Pay = () => import("@/pages/Pay");
const PaySuccess = () => import("@/pages/PaySuccess");
const Center = () => import("@/pages/Center");
const Trade = () => import("@/pages/Trade");
const MyOrder = () => import("@/pages/Center/MyOrder/MyOrder");
const GroupOrder = () => import("@/pages/Center/GroupOrder/GroupOrder");


export default [
    {
        path: '/trade',
        component: Trade,
        beforeEnter: (to, from, next) => {
            //只有从购物车才能到交易
            if (from.path === '/shopcart') {
                next()
            } else {
                next(false)
            }
        }
    },
    {
        path: '/center',
        component: Center,
        children: [
            {
                path: '/center/myOrder',
                component: MyOrder
            },
            {
                path: '/center/grouporder',
                component: GroupOrder
            },
            {
                path: '/center',
                component: MyOrder,
            }
        ]
    },
    {
        path: '/paysuccess',
        component: PaySuccess,
        beforeEnter: (to, from, next) => {
            if (from.path === '/pay') {
                next()
            } else {
                next(false);
            }
        }
    },
    {
        path: '/pay',
        component: Pay,
        beforeEnter: (to, from, next) => {
            if (from.path === '/trade') {
                next()
            } else {
                next(false);
            }
        }
    },

    {
        path: '/shopcart',
        component: ShopCart
    },
    {
        path: '/addcartsuccess',
        component: AddCartSuccess,
        beforeEnter: (to, from, next) => {
            // 路由独享守卫，只和这个路由相关的导航守卫
            let skuNum = to.query.skuNum
            let skuInfo = sessionStorage.getItem('skuInfo_key')
            if (skuNum && skuInfo) {
                next()
            } else {
                alert(111)
                next(false)
            }
        }

    },
    {
        path: '/detail/:skuId',
        component: Detail,
    },

    {
        path: '/home',
        component: Home,
        children: []
    },
    {
        path: '/login',
        component: Login,
        meta: {
            isHidden: true
        }
    },
    {
        path: '/register',
        component: Register,
        meta: {
            isHidden: true
        }
    },
    {
        name: 'Search',
        path: '/search/:keyword?',//配置路由的时候 ?号表示这个参数可传可不传，没有参数传递的时候不加上？，url路径会出现错误
        component: Search,
    },

    //访问根路径，跳转到主页
    {
        path: '/',
        redirect: '/home'
    },

]
