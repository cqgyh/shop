import Vue from "vue";
import VueRouter from "vue-router";

import routes from './routes.js'
import store from "@/store"

Vue.use(VueRouter);
//为了避免router上的push方法连续请求相同的参数会出现报错的情况，我们重写push方法
const originPush = VueRouter.prototype.push;
const originReplace = VueRouter.prototype.replace;
VueRouter.prototype.push = function (location, resolve, reject) {

    if (resolve === undefined && reject === undefined) {
        return originPush.call(this, location).catch(() => {
        });
    } else {
        return originPush.call(this, location, resolve, reject);
    }

}

VueRouter.prototype.replace = function (location, resolve, reject) {

    if (resolve === undefined && reject === undefined) {
        return originReplace.call(this, location).catch(() => {
        });
    } else {
        return originReplace.call(this, location, resolve, reject);
    }

}


const router = new VueRouter({
    mode: 'history',

    routes,
    scrollBehavior(to, from, savedPosition) {
        return {x: 0, y: 0}
    }


})
//全局路由守卫
router.beforeEach(async (to, from, next) => {
    // to  代表的是目的地的路由对象
    // from 代表的是起始页面的路由对象
    // next 代表的是一个函数，可以控制你是放行还是处理
    // next()  如果不传参代表无条件放行
    // next(false) 如果传递false代表不放行，原地待命
    // next('/') 如果传递的是一个路径或者是路由对象，那么可以强制让路由跳转到指定的位置

    const token = store.state.user.token;
    const userInfo = store.state.user.userInfo;
    if (token) {
        //如果去的是登录页面
        if (to.path === '/login') {
            //有token遇到跳转登录的 跳转到首页
            next('/')
        } else {
            if (userInfo.name) {
                //有token有userInfo的的 想去哪儿 就去哪
                // console.log('有userInfo')
                next()
            } else {
                //有token没有userInfo的 ，先获取userInfo
                try {
                    await store.dispatch('getUserInfo')
                    //获取成功的 随意跳转
                    // console.log('获取用户信息成功')
                    next()


                } catch (error) {
                    //获取失败的，我们一律当作他的token过期，跳转到登录页面，让他重新登录
                    //失败了清空token信息，重新跳转登录

                    store.dispatch('getRemoveTokenAndUserInfo')

                    next('/login')

                }

            }


        }

    } else {
        //如果token不存在，目前没有牵扯到支付相关的东西，我们全部无条件放行，后期牵扯到了，我们再加
        //83、如果没登录，访问 （交易相关、支付相关、用户中心相关）跳转到登录页面
        // 	登录后会自动跳转前面想去而没到的页面
        const targetPath=to.path
        if(targetPath.indexOf('/center')===0||targetPath.startsWith('/pay')||targetPath.startsWith('/trade')){
            // console.log('重定向')
            next(`/login?targetPath=${targetPath}`)
        }else {
            next()
        }



    }


})


export default router
