import {reqTradeInfo, reqUserAddressList} from "@/api";


const state = {
    tradeInfo:{},
    userAddressList:[]
}
const mutations = {
    SET_TRADE_INFO(state,tradeInfo){
        state.tradeInfo = tradeInfo
    },
    SET_USER_ADDRESS_LIST(state,userAddressList){
        state.userAddressList = userAddressList
    },
}
const actions = {
    //获取交易订单信息
    async getTradeInfo({commit}){
        const result = await reqTradeInfo()
        if (result.code === 200) {
            commit('SET_TRADE_INFO',result.data)
        }
    },
    //获取用户地址信息
    async getUserAddressList({commit}){
        const result = await reqUserAddressList()
        if (result.code === 200){
            commit('SET_USER_ADDRESS_LIST',result.data)
        }
    },

}
const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}
