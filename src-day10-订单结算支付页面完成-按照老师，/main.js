import Vue from 'vue'
import App from './App'

import router from '@/router';
import store from '@/store';

//mock模拟测试数据
import '@/mock/mockServer'

import TypeNav from "@/components/TypeNav/TypeNav";
import Pagination from "@/components/Pagination";
import * as api from '@/api'


// import '@/api'  //第一种测试接口请求函数
//第二种测试接口请求函数
// import {reqCategoryList} from '@/api'
// reqCategoryList()
//定义全局组件
Vue.component('TypeNav',TypeNav)
Vue.component('Pagination',Pagination)

//element-ui按需引入
import { Button,MessageBox, Message,Loading,Notification} from 'element-ui';
Vue.use(Button)


Vue.prototype.$loading = Loading.service;
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;


Vue.config.productionTip = false


new Vue({
  beforeCreate() {
    Vue.prototype.$bus=this;
    Vue.prototype.$api=api;
  },
  router,
  store,
  render: h => h(App),
}).$mount('#app')
