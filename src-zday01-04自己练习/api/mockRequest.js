import axios from "axios";
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';


const service=axios.create({
    // baseURL: '/api',
    timeout:10000,
})
service.interceptors.request.use(
    (config) => {
        NProgress.start()
        return config;
    },
    (error) => {
        alert(error.message)
    },
)
service.interceptors.response.use(
    (response) => {
        NProgress.done();


        return response.data;

    },
    (error) => {
        NProgress.done();

        //统一处理
        alert('请求失败'+error.message);
        //后续如果想继续处理的话需要返回一个失败的Promise
        // return Promise.reject(new Error(error.message))
        //后续如果不想继续处理的话 中断Promise链的传播 返回一个pending状态的promise就行
        return new Promise(() => {})

    }
)

export default service
