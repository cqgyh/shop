import ajax from './request';
import mockAjax from './mockRequest';


//3. 首页三级分类  /api/product/getBaseCategoryList  GET  无参数
export const reqCategoryList = () => ajax({
    method: 'get',
    url: '/product/getBaseCategoryList'
})


//mockAjax
export const reqBannerList = () => mockAjax({
    method: 'get',
    url: '/mock/banner'
})
export const reqFloorList = () => mockAjax({
    method: 'get',
    url: '/mock/floor'
})
