import VueRouter from "vue-router";
import Vue from "vue";
import routes from './routes'

Vue.use(VueRouter);
//重写push，replace方法，避免3.1之后的连续发送相同参数一句相同请求地址的错误提示
const originPush=VueRouter.prototype.push;
const originReplace=VueRouter.prototype.replace;

VueRouter.prototype.push=function (location,resolve,reject){
    if (resolve&&reject){
        return originPush.call(this,location,resolve,reject)
    }else {
        return originPush.call(this,location).catch(()=>{})
    }

}
VueRouter.prototype.replace=function (location,resolve,reject){
    if (resolve&&reject){
        return originReplace.call(this,location,resolve,reject)
    }else {
        return originReplace.call(this,location).catch(()=>{})
    }

}


export default new VueRouter({
    mode:'history',

    routes,

})
