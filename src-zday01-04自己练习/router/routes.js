import Home from "@/pages/Home/Home";
import Login from "@/pages/Login/Login";
import Register from "@/pages/Register/Register";
import Search from "@/pages/Search/Search";


export default [
    {
        name: "Home",
        path: '/home',
        component: Home,
    },
    {
        name: "Login",
        path: '/login',
        component: Login,
        meta: {
            isHidden: true
        }
    },
    {
        name: "Register",
        path: '/register',
        component: Register,
        meta: {
            isHidden: true
        }
    },
    {
        name: "Search",
        path: '/search/:keyword?',
        component: Search,
    },

    {
        path:'/',
        redirect:'/home'
    },
    {
        path:'*',
        redirect:'/home'
    }


]
