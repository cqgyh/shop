import Vue from 'vue'
import App from './App'

import router from '@/router'
import store from '@/store';

//mock
import '@/mock/mockServer'


import Header from "@/components/Header/Header";
import Footer from "@/components/Footer/Footer";
import MySwiper from "@/components/MySwiper/MySwiper";

//注册全局组件
Vue.component('Header',Header)
Vue.component('Footer',Footer)
Vue.component('MySwiper',MySwiper)



Vue.config.productionTip = false


new Vue({
  router,
  store,

  render: h => h(App),
}).$mount('#app')
