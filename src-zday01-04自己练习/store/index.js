import Vue from 'vue';
import Vuex from 'vuex';

import state from "@/store/state";
import mutations from "@/store/mutations";
import actions from "@/store/actions";
import getters from "@/store/getters";
import home from "@/store/modules/home";
import user from "@/store/modules/user";

Vue.use(Vuex);


export default new Vuex.Store({

    state,
    mutations,
    actions,
    getters,

    //合并小的vuex module组件合并到总的store
    modules: {
        home,
        user,
    },

})
