import Vue from 'vue'
import App from './App'

import router from '@/router';
import store from '@/store';

//mock模拟测试数据
import '@/mock/mockServer'

import TypeNav from "@/components/TypeNav/TypeNav";
import Pagination from "@/components/Pagination/Pagination";


// import '@/api'  //第一种测试接口请求函数
//第二种测试接口请求函数
// import {reqCategoryList} from '@/api'
// reqCategoryList()
//定义全局组件
Vue.component('TypeNav',TypeNav)
Vue.component('Pagination',Pagination)

Vue.config.productionTip = false


new Vue({
  beforeCreate() {
    Vue.prototype.$bus=this;
  },
  router,
  store,
  render: h => h(App),
}).$mount('#app')
