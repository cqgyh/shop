import {reqAddOrUpdateShopCart} from "@/api";

const state = {}
const mutations = {}
const actions = {
    // eslint-disable-next-line no-unused-vars
    async addOrUpdateShopCart({commit},{skuId,skuNum}){
        const result=await reqAddOrUpdateShopCart(skuId,skuNum)
        if(result.code === 200){
            // console.log('ok')
            return 'ok'

        }else {
            return Promise.reject(new Error('failed'))
        }
    }
}
const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}
