import Home from "@/pages/Home";
import Login from "@/pages/Login";
import Register from "@/pages/Register";
import Search from "@/pages/Search";
import Detail from "@/pages/Detail";
import AddCartSuccess from "@/pages/AddCartSuccess";


export default [
    {
        path: '/addcartsuccess',
        component: AddCartSuccess
    },
    {
        path: '/detail/:skuId',
        component: Detail,
    },

    {
        path: '/home',
        component: Home,
        children: []
    },
    {
        path: '/login',
        component: Login,
        meta: {
            isHidden: true
        }
    },
    {
        path: '/register',
        component: Register,
        meta: {
            isHidden: true
        }
    },
    {
        name: 'Search',
        path: '/search/:keyword?',//配置路由的时候 ?号表示这个参数可传可不传，没有参数传递的时候不加上？，url路径会出现错误
        component: Search,
    },

    //访问根路径，跳转到主页
    {
        path: '/',
        redirect: '/home'
    },

]
